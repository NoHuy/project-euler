<?php

/**
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */

echo findLargestPalindromeProduct();

function findLargestPalindromeProduct()
{
    $limit   = 100;
    $largest = 0;

    for ($i = 999; $i > $limit; $i--) {
        for ($j = $i - 1; $j > $limit; $j--) {
            $product = $i * $j;
            $sample  = (string) $product;

            if (isPalindrome($sample)) {
                $largest = ($product > $largest) ? $product : $largest;
                break;
            }
        }
    }

    return $largest;
}

function isPalindrome($text)
{
    $len    = strlen($text);
    $midPos = (int) $len / 2;
    
    $left  = substr($text, 0, $midPos);
    $right = substr($text, -$midPos);
    
    return strrev($left) == $right;
}