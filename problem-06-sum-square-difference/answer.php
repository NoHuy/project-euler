<?php

/**
 * The sum of the squares of the first ten natural numbers is,
 * 12 + 22 + ... + 102 = 385
 * 
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)2 = 552 = 3025
 * 
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 * 
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */

$range = range(1, 100);

$sumOfSquares = sumOfSquares($range);
$squareOfSum  = squareOfSum($range);

echo abs($squareOfSum - $sumOfSquares);

function sumOfSquares($numbers) {
    $squareNumbers = array_map(function($number) {
        return pow($number, 2);
    }, $numbers);

    return array_sum($squareNumbers);
}

function squareOfSum($numbers) {
    $sum = array_sum($numbers);

    return pow($sum, 2);
}